package link.shear

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import link.shear.domain.Resource
import link.shear.domain.User
import linkshear.TopPostService

@Secured(['permitAll'])
class CustomerController {
    TopPostService topPostService
    CustomerService customerService
    SpringSecurityService springSecurityService
    def index() {
        if(springSecurityService.isLoggedIn()){
            redirect(controller: 'customer',action: 'dashboard')
        }
        List<Resource> listLast = customerService.listOflastResource()
        List<Resource> listRecent = customerService.todayResource()
        render(view: '/customer/home', model: [listL: listLast, listR: listRecent])
    }

    def varificationForPasswordChange(){
        User user = customerService.searchAndNotifyValidUser(params.email)

        if(user){
            if (user.active) {
                String token = UUID.randomUUID().toString().replaceAll('-', '_')
                user.token = token
                sendMail {
                    to params.email
                    subject "linkshear password reset request"
                    html g.render(template: '/customer/updateLink', model: [token: token])
                }
                user.save(flush: true)
            }
            else {
                String message = "forgotPassword.process.user.not.activated.error"
                render(view: '/customer/forget', model: [msg:message])
            }
        }
        else {
            String message = "forgotPassword.process.no.user.found.error"
            render(view: '/customer/forget', model: [msg:message])
        }

    }
    def updatePass(){

        if(params.newpassword.equals(params.cnewpassword)) {
            boolean status = customerService.updateUserPassword(params.token, params.newpassword)
            if(status){
                List<Resource> listLast = customerService.listOflastResource()
                List<Resource> listRecent = customerService.todayResource()
                render(view: '/customer/home', model: [listL: listLast, listR: listRecent])
            }
        }
        else {
            render(view: '/customer/showUpdatePassForm', model: [token: params.token])
        }
    }

    def forget() {
    }

    def showUpdatePassForm(){

        if (customerService.isLinkValid(params.token)) {

            render(view: '/customer/showUpdatePassForm', model: [token: params.token])
        } else {

            render(view: '/customer/linkExpired')
        }


    }
    def registerUser(){
        link.shear.RegistrationCo registrationCo = new link.shear.RegistrationCo()
        User user1 =new User()
        bindData(registrationCo,params)
        registrationCo.validate()
        if(registrationCo.hasErrors()) {
            List<Resource> listLast = customerService.listOflastResource()
            List<Resource> listRecent = customerService.todayResource()
            render(view: '/customer/home', model: [errObj : registrationCo,listL: listLast, listR: listRecent])

        }
        else {
            bindData(user1,registrationCo)
            if(user1.validate()) {
                String token = UUID.randomUUID().toString().replaceAll("-","_")
                customerService.registerUser(user1,token)
                sendMail {
                    to user1.email
                    subject "linkshear account activation request"
                    html g.render(template: '/customer/activateLink', model: [token: token])
                }
                List<Resource> listLast = customerService.listOflastResource()
                List<Resource> listRecent = customerService.todayResource()
                render(view: '/customer/home', model: [mailsend: 'check your email to activate ur account',listL: listLast, listR: listRecent])
            }
        }
    }

    def activateUser() {

        boolean status = customerService.activateStatus(params.token)
        if(status){
            redirect(controller: 'customer',action: 'dashboard')
        }
        else {
            render(view: '/customer/linkExpired')
        }
    }
    def dashboard(){
        if(springSecurityService.isLoggedIn()){
            User user = (User) springSecurityService.loadCurrentUser()
            if (user.active) {
                render(view: '/customer/dashboard',model: [user: user])
            }
            else {
                redirect(controller: 'logout', action: 'index')
            }
        }
        else {
            redirect(controller: 'customer',action: 'index')
        }


    }
    def showSubs(){

    }
    def allSubs(){

    }
    def topPost(){
        List<Resource> list
        if(params.recent.equals('today')){
            list = topPostService.postToday()
        }
        if(params.recent.equals('1 week')){
            list = topPostService.postLastWeek()
        }
        if(params.recent.equals('1 month')){
            list = topPostService.postLastMonth()
        }
        if(params.recent.equals('1 year')){
            list = topPostService.postLastYear()
        }
        render(view: '/customer/_topPost',model: [list: list])
    }
}
