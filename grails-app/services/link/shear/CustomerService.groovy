package link.shear

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import link.shear.domain.Resource
import link.shear.domain.Role
import link.shear.domain.User
import link.shear.domain.UserRole

@Transactional
class CustomerService {
    SpringSecurityService springSecurityService

    def registerUser(User user, String token) {

        user.admin = false
        user.active = false
        user.token = token
        user.save()
        Role norm = Role.findByAuthority('ROLE_USER')
        println user
        println norm
        UserRole.create(user, norm)
        //springSecurityService.reauthenticate(user.username)

    }
    User searchAndNotifyValidUser(String email){

        User user = User.findByEmail(email)
        return user

    }

    def updateUserPassword(String token, String pass) {

        User us= User.findByToken(token)
        if (us) {

            us.password = pass
            us.token = null
        } else {
            println("user null")
        }
        if (us.save(flush: true, failOnError: true)) {
            return true
        } else {
            return false
        }

    }

    boolean isLinkValid(String token) {

        User user = User.findByToken(token)
        if (user) {
            return true
        } else
            return false
    }

    boolean activateStatus(String token) {

        User user = User.findByToken(token)
        if(user){
            user.active = true
            user.token = null
            user.save(flush: true)
            springSecurityService.reauthenticate(user.username)
            return true
        }
        else {
            return false
        }

    }

    List<Resource> listOflastResource(){

        List<Resource> list = Resource.list(max: 3,sort: 'id',order: 'desc')
        return list
    }

    List<Resource> todayResource() {

        def res = Resource.createCriteria()
        List<Resource> result = res.list {
            between('dateCreated', new Date() - 1, new Date())
        }
        return result
    }
}