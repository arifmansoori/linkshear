package linkshear


import grails.transaction.Transactional
import link.shear.domain.LinkResource
import link.shear.domain.Resource
import link.shear.domain.User


class TopPostService {

    static transactional = false

    def postToday() {
        def res = Resource.createCriteria()
        List<Resource> result = res.list {
            between('dateCreated',new Date()-1,new Date())
       }
        return result
    }
    def postLastWeek() {
        def res = Resource.createCriteria()
        List<Resource> result = res.list {
           between('dateCreated',new Date()-7,new Date())
        }
        return result
    }
    def postLastMonth() {
        def res = Resource.createCriteria()
        List<Resource> result = res.list {
            between('dateCreated',new Date()-30,new Date())
        }
        return result
    }
    def postLastYear() {
        def res = Resource.createCriteria()
        List<Resource> result = res.list {
            between('dateCreated',new Date()-365, new Date())
        }
        return result
    }

}
