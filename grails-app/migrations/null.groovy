databaseChangeLog = {

	changeSet(author: "arif (generated)", id: "1500464682566-1") {
		dropNotNullConstraint(columnDataType: "bigint", columnName: "document_id", tableName: "resource")
	}

	changeSet(author: "arif (generated)", id: "1500464682566-2") {
		dropNotNullConstraint(columnDataType: "bigint", columnName: "link_id", tableName: "resource")
	}
}
