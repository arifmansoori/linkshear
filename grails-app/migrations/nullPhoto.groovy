databaseChangeLog = {

	changeSet(author: "arif (generated)", id: "1499248880086-1") {
		modifyDataType(columnName: "photo", newDataType: "tinyblob", tableName: "user")
	}

	changeSet(author: "arif (generated)", id: "1499248880086-2") {
		dropNotNullConstraint(columnDataType: "tinyblob", columnName: "photo", tableName: "user")
	}
}
