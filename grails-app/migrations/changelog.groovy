databaseChangeLog = {

	changeSet(author: "arif (generated)", id: "1498801736626-1") {
		createTable(tableName: "document_resource") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "document_resoPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "file_path", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-2") {
		createTable(tableName: "link_resource") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "link_resourcePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-3") {
		createTable(tableName: "reading_item") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "reading_itemPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "is_read", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "my_resource_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-4") {
		createTable(tableName: "resource") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "resourcePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "created_by_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "document_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "las_updated", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "link_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "topic_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-5") {
		createTable(tableName: "resource_rating") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "resource_ratiPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "my_resource_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "score", type: "integer") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-6") {
		createTable(tableName: "role") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "rolePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "authority", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-7") {
		createTable(tableName: "subscription") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "subscriptionPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "seriousness", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "topic_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-8") {
		createTable(tableName: "topic") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "topicPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "created_by_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "visibility", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-9") {
		createTable(tableName: "user") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "userPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "account_expired", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "account_locked", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "active", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "admin", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "email", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "enabled", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "first_name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "last_name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "datetime") {
				constraints(nullable: "false")
			}

			column(name: "password", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "password_expired", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "photo", type: "tinyblob") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-10") {
		createTable(tableName: "user_role") {
			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "role_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-11") {
		addPrimaryKey(columnNames: "user_id, role_id", constraintName: "user_rolePK", tableName: "user_role")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-25") {
		createIndex(indexName: "FK_99gcvn81r8psmt4snq6gjgb3r", tableName: "reading_item") {
			column(name: "my_resource_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-26") {
		createIndex(indexName: "FK_rmxs9jrphvwn7tyg2n82p8wq0", tableName: "reading_item") {
			column(name: "user_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-27") {
		createIndex(indexName: "my_resource_id_uniq_1498801736528", tableName: "reading_item", unique: "true") {
			column(name: "my_resource_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-28") {
		createIndex(indexName: "FK_29on5nwfvvdmtimh40b7tnsw5", tableName: "resource") {
			column(name: "topic_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-29") {
		createIndex(indexName: "FK_blsg6m8q97cri53mkhan6h25t", tableName: "resource") {
			column(name: "document_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-30") {
		createIndex(indexName: "FK_g246684ev6hmr47bn9x3jv37f", tableName: "resource") {
			column(name: "created_by_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-31") {
		createIndex(indexName: "FK_pta289amhv5p8n2edw9x52ykq", tableName: "resource") {
			column(name: "link_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-32") {
		createIndex(indexName: "document_id_uniq_1498801736536", tableName: "resource", unique: "true") {
			column(name: "document_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-33") {
		createIndex(indexName: "link_id_uniq_1498801736536", tableName: "resource", unique: "true") {
			column(name: "link_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-34") {
		createIndex(indexName: "FK_n3bpuawgtp8q4sx9auf3bvyqg", tableName: "resource_rating") {
			column(name: "my_resource_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-35") {
		createIndex(indexName: "FK_qae5oa07q1791tfyocrc7w995", tableName: "resource_rating") {
			column(name: "user_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-36") {
		createIndex(indexName: "authority_uniq_1498801736538", tableName: "role", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-37") {
		createIndex(indexName: "FK_aojatoh2ykivd0ukh09f4mn11", tableName: "subscription") {
			column(name: "topic_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-38") {
		createIndex(indexName: "FK_tq3cq3gmsss8jjyb2l5sb1o6k", tableName: "subscription") {
			column(name: "user_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-39") {
		createIndex(indexName: "FK_gjh72tiq1cjg5r46kkx5jkghh", tableName: "topic") {
			column(name: "created_by_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-40") {
		createIndex(indexName: "username_uniq_1498801736543", tableName: "user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-41") {
		createIndex(indexName: "FK_apcc8lxk2xnug8377fatvbn04", tableName: "user_role") {
			column(name: "user_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-42") {
		createIndex(indexName: "FK_it77eq964jhfqtu54081ebtio", tableName: "user_role") {
			column(name: "role_id")
		}
	}

	changeSet(author: "arif (generated)", id: "1498801736626-12") {
		addForeignKeyConstraint(baseColumnNames: "my_resource_id", baseTableName: "reading_item", constraintName: "FK_99gcvn81r8psmt4snq6gjgb3r", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "resource", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-13") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "reading_item", constraintName: "FK_rmxs9jrphvwn7tyg2n82p8wq0", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-14") {
		addForeignKeyConstraint(baseColumnNames: "created_by_id", baseTableName: "resource", constraintName: "FK_g246684ev6hmr47bn9x3jv37f", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-15") {
		addForeignKeyConstraint(baseColumnNames: "document_id", baseTableName: "resource", constraintName: "FK_blsg6m8q97cri53mkhan6h25t", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "document_resource", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-16") {
		addForeignKeyConstraint(baseColumnNames: "link_id", baseTableName: "resource", constraintName: "FK_pta289amhv5p8n2edw9x52ykq", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "link_resource", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-17") {
		addForeignKeyConstraint(baseColumnNames: "topic_id", baseTableName: "resource", constraintName: "FK_29on5nwfvvdmtimh40b7tnsw5", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "topic", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-18") {
		addForeignKeyConstraint(baseColumnNames: "my_resource_id", baseTableName: "resource_rating", constraintName: "FK_n3bpuawgtp8q4sx9auf3bvyqg", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "resource", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-19") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "resource_rating", constraintName: "FK_qae5oa07q1791tfyocrc7w995", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-20") {
		addForeignKeyConstraint(baseColumnNames: "topic_id", baseTableName: "subscription", constraintName: "FK_aojatoh2ykivd0ukh09f4mn11", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "topic", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-21") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "subscription", constraintName: "FK_tq3cq3gmsss8jjyb2l5sb1o6k", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-22") {
		addForeignKeyConstraint(baseColumnNames: "created_by_id", baseTableName: "topic", constraintName: "FK_gjh72tiq1cjg5r46kkx5jkghh", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-23") {
		addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FK_it77eq964jhfqtu54081ebtio", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
	}

	changeSet(author: "arif (generated)", id: "1498801736626-24") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK_apcc8lxk2xnug8377fatvbn04", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	include file: 'nullPhoto.groovy'


	include file: 'null.groovy'

	include file: 'last.groovy'

	include file: 'token.groovy'
}
