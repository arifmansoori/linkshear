databaseChangeLog = {

	changeSet(author: "arif (generated)", id: "1500635558038-1") {
		addColumn(tableName: "user") {
			column(name: "token", type: "varchar(255)")
		}
	}
}
