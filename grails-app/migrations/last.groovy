databaseChangeLog = {

	changeSet(author: "arif (generated)", id: "1500471203866-1") {
		addColumn(tableName: "resource") {
			column(name: "last_updated", type: "datetime") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "arif (generated)", id: "1500471203866-2") {
		dropNotNullConstraint(columnDataType: "bigint", columnName: "link_id", tableName: "resource")
	}

	changeSet(author: "arif (generated)", id: "1500471203866-3") {
		dropColumn(columnName: "las_updated", tableName: "resource")
	}
}
