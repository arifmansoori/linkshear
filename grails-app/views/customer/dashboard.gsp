<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 29/6/17
  Time: 6:48 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="link_normal"/>
    <title>dashboard</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                 <!--add user detaisls here-->
                    <div class="row">
                    <div class="col-md-12">
                        <h4>
                            ${user.firstName}&nbsp;${user.lastName}
                        </h4>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="${createLink(controller: "customer", action: "showSubs" )}">suscription</a>
                        </div>
                        <div class="col-md-6">
                            <a href="${createLink(controller: "customer", action: "showSubs" )}">topics</a>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-sm-2" >${user.subscription.size()}</label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-sm-2" >${user.topic.size()}</label>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Inbox</b></div>
                <div class="panel-body">
                    <!--render inbox template here-->
                </div>
            </div>
        </div>
        </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="control-label col-md-8">Subscription</label>
                    <a href="${createLink(controller: 'customer',action: 'allSub')}">view all</a>
                </div>
                <div class="panel-body">
                    <!--render subscription template here-->
                </div>
            </div>
        </div>
        <!--div class="col-md-6">
            <div class="panel-heading"><b>shear link</b></div>
            <div class="panel-body">
                <render the shear-link form>
            </div>
        </div-->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Trending topics</b></div>
                <div class="panel-body">
                    <!--trending topics template-->
                </div>
            </div>
        </div>
        <!--div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Document shear</b></div>
                <div class="panel-body">
                    <!--doc shear form >
                </div>
            </div>
        </div-->
    </div>
    <!--div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Send invite</b></div>
                <div class="panel-body">
                    <!--trending topics template>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>create topic</b></div>
                <div class="panel-body">
                    <!--create topic  >
                </div>
            </div>
        </div>
    </div-->
    </div>
</body>
</html>