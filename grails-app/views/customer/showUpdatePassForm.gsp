<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 17/7/17
  Time: 1:19 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="link_normal"/>
    <title>Reset password</title>
</head>

<body>
<div class="container">
    <form class="form-horizontal"  method="post" action="updatePass">
        <div class="form-group">
            <label class="control-label col-sm-2" for="newpass">Enter&nbsp;new&nbsp;password:</label>

            <div class="col-sm-6">
                <input type="password" class="form-control" id="newpass" placeholder="Enter newpassword"
                       name="newpassword"/>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="cnewpass">Re-enter&nbsp;new&nbsp;password:</label>

            <div class="col-sm-6">
                <input type="password" class="form-control" id="cnewpass" placeholder="Re-Enter newpassword"
                       name="cnewpassword"/>
            </div>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control" id="user" name="token" value="${token}"/>
        </div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
</div>
</form>
</div>
</body>
</html>