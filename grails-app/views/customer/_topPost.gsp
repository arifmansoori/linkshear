<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 29/6/17
  Time: 6:02 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<g:each in="${list}" var="res">
<div id="box" class="row">
    <div class="col-md-4" >
        <a href="#"><img src="${resource(dir: 'images', file: 'android-logo.png')}"></a>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="row">
            <div class="col-md-8">
                ${res.createdBy.firstName}&nbsp;${res.createdBy.lastName} <small>@${res.createdBy.username}</small>
            </div>
            <div class="col-md-4">
                <a href="${createLink(controller: 'customer', action: 'topiShow')}">${res.topic.name}</a>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    ${res.description}
                </div>
            </div>
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <a href="${createLink(controller: 'customer',action: 'viewPost')}">view post</a>
                </div>
            </div>
        </div>
    </div>
</div>
</g:each>
</body>
</html>