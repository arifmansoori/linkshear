<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 27/6/17
  Time: 1:56 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="link_normal"/>
    <title>home page</title>
   </head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Recent posts</b></div>

                <div class="panel-body">
                    <g:render template="topPost" model="${[list:listL]}"/>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Login</b></div>

                <div class="panel-body">
                    <g:render template="loginForm">
                    </g:render>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label id="top" class="control-label col-md-8">top posts</label>
                    <g:select id="recent" value="" from="${['today','1 week','1 month','1 year']}" class="btn" name="time"  onchange="changeTopPost(this.value)">
                    </g:select>
                    <!--${remoteFunction(controller: 'customer', action: 'topPost', update: 'top-post',params: '\'recent=\'+this.value')}-->
                </div>
                <div id="top-post" class="panel-body">
                    <g:render template="topPost" model="${[list: listR]}">
                    </g:render>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Register</b></div>

                <div class="panel-body">
                    <g:render template="registerForm"></g:render>
                    ${mailsend}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    function changeTopPost(val) {
        $.ajax({
         url:'${createLink(controller: 'customer',action: 'topPost')}',
         data : { recent: val },
         success : function (responce) {
             document.getElementById('top-post').innerHTML = responce;

         }
         });
    }

</script>
</body>
</html>