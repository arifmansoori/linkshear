<%@ page import="link.shear.domain.User" %>
<form class="form-horizontal" method="post"  action="registerUser" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label col-sm-2" for="firstName">First&nbsp;name:</label>
        <div class="col-sm-5">
            <input  type="text" class="form-control" id="firstName" placeholder="Enter first name" name="firstName" value="${fieldValue(bean: errObj, field: 'firstName')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" as="list" field="firstName"/>
        </div>
    </div>
    <div class="form-group  ">
        <label class="control-label col-sm-2" for="lastName">Last&nbsp;name:</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" id="lastName" placeholder="Enter Last name" name="lastName" value="${fieldValue(bean: errObj, field: 'lastName')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" as="list" field="lastName"/>
        </div>
    </div>
    <div class="form-group  ">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-5">
            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="${fieldValue(bean: errObj, field: 'email')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" field="email"/>
        </div>
    </div>
    <div class="form-group  ">
        <label class="control-label col-sm-2" for="userName">user&nbsp;name:</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" id="userName" placeholder="Enter user name" name="username" value="${fieldValue(bean: errObj,field: 'username')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" field="username"/>
        </div>
    </div>
    <div class="form-group  ">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password" value="${fieldValue(bean: errObj, field: 'password')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" field="password"/>
        </div>
    </div>
    <div class="form-group  ">
        <label class="control-label col-sm-2" for="cpwd">confirm password:</label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="cpwd" placeholder="Re-enter your password" name="cpassword" value="${fieldValue(bean: errObj, field: 'cpassword')}">
        </div>
        <div class="col-sm-4 text-danger">
            <g:renderErrors bean="${errObj}" field="cpassword"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="photo">Upload image:</label>
        <div class="col-sm-5">
            <input type="file" class="form-control" id="photo"  name="photo">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>