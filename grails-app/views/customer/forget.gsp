<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 12/7/17
  Time: 12:15 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="link_normal"/>
    <title>Reset password</title>
</head>

<body>
<div class="container">
    <form class="form-horizontal" method="post" action="varificationForPasswordChange">
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Enter&nbsp;registered&nbsp;Email:</label>
            <div class="col-sm-4">
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"/>
            </div>
            <div class="control-label col-sm-4 text-danger">
                <g:if test="${msg}">
                    <g:message code="${msg}"/>
                </g:if>
            </div>
            </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">submit</button>
            </div>
        </div>
        </form>
</div>
</body>
</html>