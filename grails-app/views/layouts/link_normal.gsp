<%--
  Created by IntelliJ IDEA.
  User: arif
  Date: 27/6/17
  Time: 1:19 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title><g:layoutTitle default="Link shear"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'badge.png')}" type="image/x-icon">
    <link rel="badge" href="${assetPath(src: 'badge.png')}">
    <link rel="badge" sizes="114x114" href="${assetPath(src: 'badge.png')}">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

    <asset:stylesheet src="style.css"/>
    <asset:stylesheet src="bootstrap.css"/>
    <asset:javascript src="application.js"/>
    <style>
body {

    line-height: 1.8em;
    background-color: #fff;
    background-image: url("${resource(dir: 'images',file: '02.gif')}");
    background-repeat: repeat;
    color: #666;
}
#banner {
    width:100%;
    height:150px;
    background:#bbd9ee url("${resource(dir: 'images',file: 'banner.jpg')}") top center no-repeat;
}
</style>
    <g:layoutHead/>
</head>

<body>
<div id="banner">
    <p><a href="#"><asset:image src="home.gif" alt="homepage"/></a> | <a href="arif@fintechlabs.in"><asset:image
            src="mail.gif" alt="contact"/></a></p>

    <h1><strong>Link shear.</strong></h1>
</div>

<div id="menu">
    <sec:ifLoggedIn>
    <ul id="nav">
        <li id="home"><a href="#">Create topic </a></li>
        <li id="who"><a href="#">Send invite</a></li>
        <li id="prod"><a href="#">Create resource</a></li>
        <li id="serv"><a href="${createLink(controller: 'logout', action: 'index')}">logout</a></li>
        <li class="dropdown" id="cont">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">${sec.username()}
            <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Profile</a></li>
                <li><a href="#">Users</a></li>
                <li><a href="#">Topic</a></li>
                <li><a href="#">Posts</a></li>
                <li><a href="#">Logout</a></li>
            </ul>
        </li>

    </ul>
    </sec:ifLoggedIn>
</div>

<div id="container">
    <g:layoutBody/>
</div>

<div id="footer">
    <p><a href="#">LinkShear</a> | <a href="arif@fintechlabs.in">contact</a> | &copy; 2017 Arif | Design by <a
            href="http://www.fintechlabs.in">www.fintechlabs.in</a> | Licensed under a <a rel="license"
                                                                                          href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 License</a>
    </p>
</div>
</body>
</html>
