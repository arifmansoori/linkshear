package link.shear.domain

class Subscription {

    Date dateCreated
    link.shear.domain.Seriousness seriousness

    static belongsTo = [user: User,topic: Topic]

    static constraints = {

    }
}
