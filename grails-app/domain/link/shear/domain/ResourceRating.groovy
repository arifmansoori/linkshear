package link.shear.domain

class ResourceRating {

    Integer score
    Resource myResource

    static belongsTo = [user: User]

    static constraints = {
    }
}
