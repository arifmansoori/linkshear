package link.shear.domain

class Topic {
    String name
    Date dateCreated
    Date lastUpdated
    link.shear.domain.Visibility visibility
    static hasMany = [subscription: Subscription, myResource: Resource]
    static belongsTo = [createdBy: User]

    static constraints = {
    }
}
