package link.shear.domain


class Resource {

    String description
    Date lastUpdated
    Date dateCreated

    static hasMany = [readingItem: ReadingItem]
    static belongsTo = [createdBy: User, topic: Topic, link: LinkResource, document: DocumentResource]
    static constraints = {
        link nullable: true
        document nullable: true

    }
}
