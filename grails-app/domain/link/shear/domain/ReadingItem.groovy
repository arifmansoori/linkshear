package link.shear.domain

class ReadingItem {

    boolean isRead

    static belongsTo = [user: User, myResource: Resource]
    static constraints = {
    }
}
