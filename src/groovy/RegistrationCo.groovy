package link.shear

import grails.validation.Validateable
import link.shear.domain.User


/**
 * Created by arif on 30/6/17.
 */
@Validateable
class RegistrationCo {
    def customerService

    String firstName
    String lastName
    String username
    String email
    String password
    String cpassword
    //byte[] photo

    static constraints = {

        firstName(blank: false, validator: {val ->
            if(val.matches('[a-zA-Z]+')){
                return true
            }
            else{
                return 'registrationCo.firstName.number.error'
            }

        })

        lastName(blank: false, validator: {val ->
            if(val.matches('[a-zA-Z]+')){
                return true
            }
            else{
                return 'registrationCo.lastName.number.error'
            }
        })
        username(blank: false, maxSize: 10, validator: { val ->
            User user = User.findByUsername(val)
            if(!(user)){
                return true
            }
            else{
                return "registrationCo.username.notunique.error"
            }
        })
        password(blank: false, minSize: 8,validator: {val ->
            if(val.matches("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?!.*[\\s+\\-!%?\$(){}|\\[\\]=\'\"<>,./`~]).{8,}\$")){
                true
            }
            else{
                return "registrationCo.password.strengh.error"
            }

        })
        email(blank: false,email: true, validator: { val ->
            User user = User.findByEmail(val)
            if(!(user)){
                return true
            }
            else{
                return "registrationCo.email.notunique.error"
            }
        })
        cpassword(validator: { val, obj->
            if(val.equals(obj.password)){
                true
            }
            else{
                return 'registrationCo.cpassword.notMatch.error'
            }

        })
    }
}
