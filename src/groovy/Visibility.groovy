/**
 * Created by arif on 27/6/17.
 */
package link.shear.domain

public enum Visibility {
    PUBLIC('public'),
    PRIVATE('private')
    String id

    Visibility(String id) {
        this.id = id
    }

}